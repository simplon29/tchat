#------------------------------------------------------------
#        Script MySQL.
#------------------------------------------------------------


#------------------------------------------------------------
# Table: Utilisateur
#------------------------------------------------------------

CREATE TABLE Utilisateur(
        Id       Int  Auto_increment  NOT NULL ,
        Password Varchar (50) NOT NULL ,
        Login    Varchar (50) NOT NULL
	,CONSTRAINT Utilisateur_PK PRIMARY KEY (Id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: message
#------------------------------------------------------------

CREATE TABLE message(
        Id             Int  Auto_increment  NOT NULL ,
        Message        Varchar (50) NOT NULL ,
        Date           Date NOT NULL ,
        Id_Utilisateur Int NOT NULL
	,CONSTRAINT message_PK PRIMARY KEY (Id)

	,CONSTRAINT message_Utilisateur_FK FOREIGN KEY (Id_Utilisateur) REFERENCES Utilisateur(Id)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Lire
#------------------------------------------------------------

CREATE TABLE Lire(
        Id             Int NOT NULL ,
        Id_Utilisateur Int NOT NULL
	,CONSTRAINT Lire_PK PRIMARY KEY (Id,Id_Utilisateur)

	,CONSTRAINT Lire_message_FK FOREIGN KEY (Id) REFERENCES message(Id)
	,CONSTRAINT Lire_Utilisateur0_FK FOREIGN KEY (Id_Utilisateur) REFERENCES Utilisateur(Id)
)ENGINE=InnoDB;

