<?php
session_start();
$bdd = new PDO('mysql:host=localhost;dbname=tchat;charset=utf8', 'root', '');
if(isset($_POST['envoyer'])){
    if(!empty($_POST['message'])){
        $pseudo = $_SESSION['pseudo'];

        // nl2br permet à l'utilisateur d'effectuer des sauts de ligne
        $message = nl2br(htmlspecialchars($_POST['message']));

        // envoyer message:
        $insererMessage = $bdd->prepare('INSERT INTO message(Pseudo, Message, Date, Lu) VALUES(?, ?, NOW(), 0)');
        $insererMessage->execute(array($pseudo, $message));

    }else{
        echo'Veuillez completer tout les champs';
    }
}
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- script jQuery pour avoir l'effet de message instantané -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.3/jquery.min.js"></script>
    <link rel="stylesheet" href="css/style.css">
    <title>tchat</title>
</head>
<body>
<div id="conteneur" class="conteneur">
    <div id="messages" class="messages">
        <section id="messages"></section> 
        <script>
            setInterval('load_message()',500);
            function load_message(){
                $('#messages').load('loadMessages.php');
            }
        </script>
    </div>
    <div id="post" class="post">
        <form method="POST" action="">
            <textarea name="message" id="message" cols="30" rows="5" placeholder="votre message" class="new_message"></textarea>
            <input type="submit" name="envoyer" value="Envoyer" class="new_submit">
        </form>
        <a href="deconnexion.php">
        <button>Se déconnecter</button>
        </a>
    </div>

</div>
</body>
</html>