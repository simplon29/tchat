<?php
session_start();
$bdd = new PDO('mysql:host=localhost;dbname=tchat;charset=utf8', 'root', '');
$recupMessages = $bdd->query('SELECT * FROM message');

while($message = $recupMessages->fetch()){
    if($message['Pseudo'] == $_SESSION['pseudo']){
        ?>
        <p class="M_date"><?= $message['Date'] ?></p>
        <div id="m14" class="M M_envoye">
            <h3><?= $message['Pseudo'];?></h4>
            <p class="M_message"><?= $message['Message'];?></p>
        </div>
    <?php
    }else{
    ?>
        <p class="M_date"><?= $message['Date'] ?></p>
        <div id="m14" class="M M_recu">
            <h3><?= $message['Pseudo'];?></h4>
            <p class="M_message"><?= $message['Message'];?></p>
        </div>
    <?php
    }
}
?>