<?php
session_start();
$bdd = new PDO('mysql:host=localhost;dbname=tchat;charset=utf8', 'root', '');
if(isset($_POST['envoi'])){
    if(!empty($_POST['pseudo'])&& !empty($_POST['mdp'])){
        $pseudo = htmlspecialchars($_POST['pseudo']);
        $mdp = sha1($_POST['mdp']);
        $recupUser = $bdd->prepare('SELECT * FROM utilisateur WHERE Pseudo = ? AND Password = ?');
        $recupUser->execute(array($pseudo, $mdp));
        if($recupUser->rowCount() > 0){
            $_SESSION['pseudo'] = $pseudo;
            $_SESSION['mdp'] = $mdp;
            $_SESSION['id'] = $recupUser->fetch()['Id'];
            header('Location: index.php');
            exit;
        }else{
            echo 'Votre mot de passe et/ou pseudo est incorrect';
        }
    }else{
        echo 'Veuillez renseigner tout les champs';
    }

}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Connexion</title>
</head>
<body>
    <form method="POST" action="" align="center">
        <h3>Connectez vous pour discuter !</h3>
        <p>Pseudo:<input type="text" name="pseudo" autocomplete="off"></p>
        <p>Password:<input type="password" name="mdp" autocomplete="off"></p>
        <input type="submit" name="envoi" value="Connexion">
    </form>
    <a href="inscription.php">
        <button>S'inscrire</button>
    </a>

</body>
</html>