<?php
session_start();
// connexion à la base de donnée en créant un objet PDO/ nom utilisateur / mdp
$bdd = new PDO('mysql:host=localhost;dbname=tchat;charset=utf8', 'root', '');
// si on clique sur creer:
if(isset($_POST['envoi'])){
    // si les champs ne sont pas vides:
    if(!empty($_POST['pseudo']) AND !empty($_POST['mdp'])){
        // on declare les variables, on encode avec htmlspecialchars pour la sécurité pour éviter que des rigolos injecte du code malveillant
        $pseudo = htmlspecialchars($_POST['pseudo']);
        // encodage du mdp en sha1 pour crypté les données
        $mdp = sha1($_POST['mdp']);

        // inserer l'utilisateur dans la bdd, comme on a créé l'objet PDO on peut utilisé des requêtes comme prepare, execute, etc...
        $insertUser = $bdd->prepare('INSERT INTO utilisateur(Pseudo, Password) VALUES(?,?)');
        $insertUser->execute(array($pseudo, $mdp));

        // on démarre une session et on récupère l'utilisateur dans la bdd pour pouvoir récupérer son id avec la methode fetch
        $recupUser = $bdd->prepare('SELECT * FROM utilisateur WHERE Pseudo = ? AND Password = ?');
        $recupUser->execute(array($pseudo, $mdp));
        if($recupUser->rowCount() > 0){
            $_SESSION['pseudo'] = $pseudo;
            $_SESSION['mdp'] = $mdp;
            $_SESSION['id'] = $recupUser->fetch()['Id'];
        }
        header('Location:connexion.php');
        exit;
    }else{
        echo "Veuillez remplir tout les champs";
    }

}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Inscription</title>
</head>
<body>
    <form method="POST" action="" align="center">
        <h3>Créer un compte si vous voulez discuter</h3>
        <p>Ne vous inquietez pas, votre mot de passe est crypté :)</p>
        <p>Pseudo:<input type="text" name="pseudo" autocomplete="off"></p>
        <p>Password:<input type="password" name="mdp" autocomplete="off"></p>
        <input type="submit" name="envoi" value="Créer">
    </form>
    <p>Vous avez déjà un compte ?</p>
    <a href="connexion.php">
        <button>connexion</button>
    </a>
    
</body>
</html>