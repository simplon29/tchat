<?php

class UtilisateurRepository {

  private $db;

  // On commence par créer la connexion :

  public function __construct(){
    $this->db = new Database();
    $this->db = $this->db->getBDD();
  }

  ///////////////
  // METHODES  //
  ///////////////

  /**
 * Methode permettant de récupérer un objet utilisateur construit.
 * @param  mixed $utilisateur  Soit l'ID de l'utilisateur (int), soit le nom de l'utilisateur (string)
 * @return utilisateur        Retourne un utilisateur instancié.
 */
  public function getUtilisateur($utilisateur) {
    // En fonction de si on nous donne l'id ou le nom, on cherche la ligne de l'utilisateur dans la DB.
    if (is_string($utilisateur)) {
      $sql = "SELECT * FROM utilisateur WHERE Login = :Login ;";
    }else{
      $sql = "SELECT * FROM utilisateur WHERE Id = :Id ;";
    }
    $requete = $this->db->prepare($sql);

    $requete->execute([':Login'=>$utilisateur]);

    $infos = $requete->fetch(PDO::FETCH_ASSOC);

    // Une fois qu'on l'a récupéré, on modifie le tableau. En effet, les clés dans $infos ont les noms des colonnes : Id, Login ... Or, pour construire un objet, la méthode hydrate a besoin des attributs, sans le "_Id". Il faut donc reconstruire le tableau, en gardant juste le début des clés : id, login, ...

    $utilisateur = [];
    foreach($infos as $key => $value){
      $key = explode('_',$key);
      $key = $key[0];
      $utilisateur = array_merge($utilisateur,[$key => $value]);

    }

    // On construit l'utilisateur' :
    $utilisateur = new $utilisateur['Login']($utilisateur);
    return $utilisateur;
  }


  /**
   * Permet de créer un utilisateur, si le nom proposé est libre.
   * @param  string $Login  Le nom de l'utilisateur
   * @param  string $Password Le password de l'utilisateur
   * @return string       Un message de réussite ou d'erreur selon.
   */
  public function createUtilisateur(string $Login, string $Password){


    $sql = 'SELECT Login FROM `utilisateur` WHERE Login = "'.$Login.'"; ';
    $Loginpresent = $this->db->query($sql);
    $Loginpresent = $Loginpresent->fetch(PDO::FETCH_ASSOC);

    if (!$Loginpresent) {
    $sql = "INSERT INTO utilisateur (`Login`, `Password`) VALUES (:Login,:Password)";

    $requete = $this->db->prepare($sql);

    $requete->execute([ ':Login'=>$Login,
                        ':Password'=>$Password,
                        ]);

    return "L'utilisateur' $Login a été ajouté à la Base de Données.";
    } else if($Loginpresent['Login'] == $Login) {
    return "<p style='color:red;'>Ce pseudo est déjà pris.</p>";
    }
}
  // Delete

  /**
   * Permet de supprimer un utilisateur de la BDD
   * @param  int    $Id l'Id de l'utilisateur à supprimer
   * @return string           message de validation
   */
  public function deleteUtilisateur(int $Id){
    $sql = "DELETE FROM utilisateur WHERE Id = :Id ;";

    $suppression = $this->db->prepare($sql);
    $suppression->execute([':Id'=>$Id]);

    return "utilisateur supprimé";
  }

  

}



