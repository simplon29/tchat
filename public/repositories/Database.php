<?php

/**
 * Classe permettant la connexion à la BDD
 */
class Database {
  // Définir les attributs
  const DB_HOST = "localhost";
  const DB_BASE = "tchat";
  const DB_USER = "root";
  const DB_MDP = "";
  const DB_TABLE = "utilisateur";

  private $_BDD;

  public function __construct(){
    $this->connectBDD();
  }

  /**
   * Permet d'initialiser la connexion à la Base de Données
   * @return un objet PDO en cas de succès, ou un message d'erreur.
   */
  private function connectBDD(){
    try {
      $this->_BDD = new PDO("mysql:host=".self::DB_HOST.";dbname=".self::DB_BASE,self::DB_USER,self::DB_MDP, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
    } catch (PDOException $e) {
      die("Erreur de connexion : " . $e->getMessage());
    }
  }

  public function getBDD(){
    return $this->_BDD;
  }

  // Méthodes propres à la Base de données de manière générale :

  /**
   * Fonction qui permet de retourner tous les utilisateurs stockés dans la Base de Données
   * @return Array Tableau contenant tous les utilisateurs.
   */
  public function getAllUtilisateurs(){
    $sql = 'SELECT * FROM '.self::DB_TABLE;
    $requete =  $this->_BDD->query($sql);
    $resultat = $requete->fetchAll(PDO::FETCH_OBJ);

    return $resultat;
  }

  public function initialisationBDD(){
    $verif = $this->_BDD->query("SHOW TABLES LIKE '".self::DB_TABLE ."'");
    $verif = $verif->fetchAll(PDO::FETCH_ASSOC);
      if (empty($verif) || in_array(self::DB_TABLE,$verif[0])){
        $sql = "CREATE TABLE IF NOT EXISTS `utilisateur` (
            `Id` int(11) NOT NULL AUTO_INCREMENT,
            `Login` varchar(50) NOT NULL,
            `Password` varchar(20) NOT NULL,
            PRIMARY KEY (`Id`)
          ) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;";

        $this->_BDD->query($sql);

        echo "La table ".self::DB_TABLE." a été créée.";

      }
  }

  public function suppressionTable(){
    $verif = $this->_BDD->query("SHOW TABLES LIKE '".self::DB_TABLE ."'");
    $verif = $verif->fetchAll(PDO::FETCH_ASSOC);

    if (!empty($verif) && in_array(self::DB_TABLE,$verif[0])){
      $sql = "DROP TABLE IF EXISTS utilisateur";

      $this->_BDD->query($sql);

      echo "La table ".self::DB_TABLE." a été supprimée.";
    }
  }
}
