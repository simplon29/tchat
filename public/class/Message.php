<?php


class Message {
    private $_Id, $_Id_Utilisateur, $_Message, $_Date;

    public function __construct(Array $infos){
        // hydrater les objets
        $this->hydrate($infos);
    }
    private function hydrate(Array $infos){
        // recuperer deux valeurs, une clé et une valeur
        foreach($infos as $key => $value){
            $methode = "set".ucfirst($key); //cette ligne permet de savoir quelle setter appeler
            if(method_exists($this, $methode)){
                $this->$methode($value);
            }
        };
    }
    public function getId(){
        return $this->_Id;
    }
    private function setId(int $Id){
        $this->_Id = (int) $Id;
    }


    public function getId_Utilisateur(){
        return $this->_Id_Utilisateur;
    }
    private function setId_Utilisateur(int $Id_Utilisateur){
        $this->_Id_Utilisateur = (int) $Id_Utilisateur;
    }


    public function getMessage(){
        return $this->_Message;
    }
    private function setMessage(string $Message){
        $Titre = htmlspecialchars($Message);
        $this->_Message = $Message;
    }

    public function getDate(){
        return $this->_Date;
    }
    private function setDate(int $Date){
        $this->_Date = (int) $Date;
    }

}



?>