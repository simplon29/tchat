<?php

class Lire {
    private $_Id, $_Id_Utilisateur;

    public function __construct(array $infos){
        // hydrater les objets
        $this->hydrate($infos);
    }
    private function hydrate(Array $infos){
        // recuperer deux valeurs, une clé et une valeur
        foreach($infos as $key => $value){
            $methode = "set".ucfirst($key); //cette ligne permet de savoir quelle setter appeler
            if(method_exists($this, $methode)){
                $this->$methode($value);
            }
        };
    }
    public function getId(){
        return $this->_Id;
    }
    private function setId(int $Id){
        $this->_Id = (int) $Id;
    }
    public function getId_Utilisateur(){
        return $this->_Id_Utilisateur;
    }
    private function setId_Utilisateur(int $Id_Utilisateur){
        $this->_Id_Utilisateur = (int) $Id_Utilisateur;
    }
}


?>