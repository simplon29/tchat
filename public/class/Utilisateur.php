<?php

class Utilisateur {
    //attributs
    private $_Id, $_Login, $_Password;



    // constructeur
    public function __construct(Array $infos){
        // hydrater les objets
        $this->hydrate($infos);
    }

    // méthodes
    private function hydrate(Array $infos){
        // recuperer deux valeurs, une clé et une valeur
        foreach($infos as $key => $value){
            $methode = "set".ucfirst($key); //cette ligne permet de savoir quelle setter appeler
            if(method_exists($this, $methode)){
                $this->$methode($value);
            }
        };
    }


    public function getId(){
        return $this->_Id;
    }
    private function setId(int $Id){
        $this->_Id = (int) $Id;
    }


    public function getLogin(){
        return $this->_Login;
    }
    private function setGenre($Login){
        $this->_Login = $Login;
    }


    public function getPassword(){
        return $this->_Password;
    }
    private function setPassword($Password){
        $this->_Password = $Password;
    }

}






?>